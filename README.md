# K8S-Template-Repo
The directory structure for K8s config files

## Installation

Copy the k8s-config folder in your source repo

```bash
git@gitlab.com:yogesh.golande/k8s-template-repo.git

OR

https://gitlab.com/yogesh.golande/k8s-template-repo.git
```

## Usage

```python
# deployments
all the {microservice}-deployment.yaml files must be added here.

# services
all the {microservice}-service.yaml files must be added here.

# ingress
all the {name}-ingress.yaml files must be added here.

# config
all the {microservice}-configmap.yaml or {name}-configmap.yaml files must be added here.

# secrets
all the {microservice}-secrets.yaml or {name}-secrets.yaml files must be added here.
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

